import { inject } from "vue";

export function useSwal(): Swal {
    const swal = inject("$swal") as any

    return {
        success: (message, popup = false) => {
            swal.fire({
                toast: !popup,
                position: popup ? 'center' : 'bottom-end',
                showConfirmButton: popup,
                timer: popup ? undefined : 3000,
                timerProgressBar: false,
                icon: 'success',
                title: message,
                confirmButtonText: 'OK'
            })
        },
        error: (message, popup = false) => {
            swal.fire({
                toast: !popup,
                position: popup ? 'center' : 'bottom-end',
                showConfirmButton: popup,
                timer: popup ? undefined : 3000,
                timerProgressBar: false,
                icon: 'error',
                title: message,
                confirmButtonText: 'OK'
            })
        },
        confirm: (title, message, onOk, onCancel) => {
            swal.fire({
                title: title,
                text: message,
                icon: 'question',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
            }).then((r: any) => {
                if (r.isConfirmed) {
                    onOk()
                }
                else {
                    if (onCancel) onCancel()
                }
            })
        },
    }
}

interface Swal {
    success: (message: string, popup?: boolean) => void,
    error: (message: string, popup?: boolean) => void,
    confirm: (title: string, message: string, onOk: () => void, onCancel?: () => void) => void,
}