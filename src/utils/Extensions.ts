/* eslint-disable no-extend-native */

/*
FUngsi-fungsi extension yang sering dipakai di taruh di sini
 */

import moment, {Moment} from "moment";
import {BASE_URL} from "@/api/Axios";

declare global {
    interface String {
        toMoment(): Moment,
        getDateDiff(time?: Moment): number,

        toSqlDateTime(): string,
        toDateIndo(): string,
        toDateNormal(): string,
        toTimeNormal(): string,
        toDateTimeNormal(): string,

        capitalize(): string,
        getLastPath(): string,
        getExtension(): string,
        asUrl(): string,
        asUrlOrBase64(): string,
    }

    interface Date {
        toSqlDateTime(): string,
    }

    interface Number {
        toHourMinuteSecond(optionalHour?: boolean): string,
        toHourMinuteSecondText(): string,
    }

    interface File {
        toBase64(): Promise<string>,
        createPreviewUrl(): string,
    }
}

/*
Konversi dari date string menjadi moment, sekalian menyesuaikan timezone nya
 */
String.prototype.toMoment = function (): Moment {
    const offset = new Date().getTimezoneOffset() + 420
    return moment(String(this)).add(offset, 'minute')
}

/*
Menghitung perbedaan waktu, default parameter waktu saat ini
 */
String.prototype.getDateDiff = function(time: Moment = moment()): number {
    const n = Math.floor(moment.duration(String(this).toMoment().diff(time)).asSeconds())

    return n < 0 ? 0 : n
}

String.prototype.toSqlDateTime = function (): string {
    const mm = String(this).toMoment()

    return mm.format('YYYY-MM-DD HH:mm:ss')
}

Date.prototype.toSqlDateTime = function (): string {
    const mm = moment(this)

    return mm.format('YYYY-MM-DD HH:mm:ss')
}

/*
Ngeformat date string menjadi format indo Contoh: 12 Juni 2021
 */
String.prototype.toDateIndo = function (): string {
    const mm = String(this).toMoment()

    const d = mm.format('DD')
    const m = mm.format('M')
    const y = mm.format('YYYY')

    return d + " " + bulans[parseInt(m) - 1] + " " + y
}

/*
Ngeformat date string menjadi : 12/06/2021
 */
String.prototype.toDateNormal = function (): string {
    const mm = String(this).toMoment()

    return mm.format('DD/MM/YYYY')
}

/*
Ngeformat date string menjadi : 12.00
 */
String.prototype.toTimeNormal = function (): string {
    const mm = String(this).toMoment()

    return mm.format('HH.mm')
}

/*
Ngeformat date string menjadi : 12/06/2021 12.00
 */
String.prototype.toDateTimeNormal = function (): string {
    const mm = String(this).toMoment()

    return mm.format('DD/MM/YYYY HH:mm')
}

/*
Mengkapitalisasi huruf pertama. Contoh : dari "aku bisa" menjadi "Aku Bisa"
 */
String.prototype.capitalize = function (): string {
    const s = String(this)

    if (!s) return ''

    return s.split(' ')
        .map(x => x.charAt(0).toUpperCase() + x.substring(1).toLowerCase())
        .join(' ')
}

/*
Mengambil last path. Contoh : dari "http:/blabla/semau/kamu" menjadi "kamu"
 */
String.prototype.getLastPath = function (): string {
    const s = String(this)

    if (!s) return ''

    const ar = s.split('/')
    return ar[ar.length - 1]
}

/*
Mengambil extension
 */
String.prototype.getExtension = function (): string {
    const s = String(this)

    if (!s) return ''

    const ar = s.split('.')
    return ar[ar.length - 1]
}

String.prototype.asUrl = function (): string {
    const s = String(this)

    if (s.startsWith('/'))
        return BASE_URL + s.substring(1)
    else
        return BASE_URL + s
}

String.prototype.asUrlOrBase64 = function (): string {
    const s = String(this)

    if (s.startsWith('data:'))
        return s
    if (s.startsWith('/'))
        return BASE_URL + s.substring(1)
    else
        return BASE_URL + s
}

const bulans = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
]

/*
Memformat dari angka (detik) menjadi format jam:menit:detik. Contoh: "01:01:00"
 */
Number.prototype.toHourMinuteSecond = function (optionalHour: boolean = true): string {
    let n = Number(this)

    if (n < 0) n = 0

    const hour = Math.floor(n / 3600)
    const minute = Math.floor((n % 3600) / 60)
    const second = n % 60

    const hourText = hour.toString().padStart(2, "0")
    const minuteText = minute.toString().padStart(2, "0")
    const secondText = second.toString().padStart(2, "0")

    if (optionalHour && hour === 0)
        return minuteText + ":" + secondText
    else
        return hourText + ":" + minuteText + ":" + secondText
}

/*
Memformat dari angka (detik) menjadi format n Jam m Menit i Detik. Contoh: "1 Jam 30 menit 4 detik"
 */
Number.prototype.toHourMinuteSecondText = function (): string {
    let n = Number(this)

    if (n < 0) n = 0

    const hour = Math.floor(n / 3600)
    const minute = Math.floor((n % 3600) / 60)
    const second = n % 60

    const hourText = hour ? hour + " jam " : ""
    const minuteText = minute ? minute + " menit " : ""
    const secondText = second + " detik"

    return hourText + minuteText + secondText
}

File.prototype.toBase64 = function (): Promise<string> {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsDataURL(this)
        reader.onload = () => {
            if (reader.result && typeof reader.result === 'string')
                resolve(reader.result)
            else
                reject("Cannot read base64")
        }
        reader.onerror = error => reject(error)
    });
}

File.prototype.createPreviewUrl = function (): string {
    return URL.createObjectURL(this)
}

export {}