import exportFromJSON from "export-from-json";

export default function TableExport(data: Array<any>, opts: any = {}) {

    const __options = Object.assign({
        fileName: 'filename',
        type: 'csv',
        withBOM: false,
        useFormatter: true,
        delimiter: ',',
    }, opts)

    return new Promise((resolve, reject) => {
        try {
            exportFromJSON({
                data: data,
                fileName: __options.fileName,
                exportType: __options.type,
                withBOM: __options.withBOM,
                delimiter: __options.delimiter,
                fields: {}
            });

            resolve('Success')
        } catch (error) {
            reject(error)
        }
    })
}

export function extractData(data: Array<any>, columns: Array<string>) {
    return data.map(x => {
        const obj = {} as any

        for (const col of columns) {
            const part = col.split('.')

            if (part.length == 1) {
                if (x[col] != undefined)
                    obj[col] = x[col]
            }
            else {
                let d = x as any

                for (let a = 0; a < part.length; a++) {
                    d = d[part[a]]

                    if (d == undefined) break
                }

                if (d != undefined) {
                    obj[part.join('-')] = d
                }
            }
        }

        return obj
    })
}