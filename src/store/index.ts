import {createStore} from "vuex";
import AccountTypeModel from "@/models/AccountTypeModel";

export default createStore({
    state: {
        apiCallCount: 0,
        accountType: null as AccountTypeModel | null
    },
    mutations: {
        incrementApiCall(state) {
            state.apiCallCount = state.apiCallCount + 1
        },
        decrementApiCall(state) {
            state.apiCallCount = state.apiCallCount - 1
        },
        setAccountType(state, accountType: AccountTypeModel) {
            state.accountType = accountType
        }
    },
    actions: {
        incrementApiCall(context) {
            context.commit('incrementApiCall')
        },
        decrementApiCall(context) {
            context.commit('decrementApiCall')
        },
        setAccountType(context, accountType: AccountTypeModel) {
            context.commit('setAccountType', accountType)
        }
    },
    modules: {},
});
