import {createRouter, createWebHistory, RouteRecordRaw} from "vue-router";
import Home from "../views/Home.vue";
import Dashboard from "@/views/Dashboard.vue";
import Login from "@/views/Login.vue";
import ClientPage from "@/views/client/ClientPage.vue";
import ContactSetPage from "@/views/contact/set/ContactSetPage.vue";
import ContactPage from "@/views/contact/detail/ContactPage.vue";
import BroadcastPage from "@/views/broadcast/BroadcastPage.vue";
import InputBroadcastPage from "@/views/broadcast/InputBroadcastPage.vue";
import DetailBroadcastPage from "@/views/broadcast/DetailBroadcastPage.vue";
import OperatorPage from "@/views/operator/OperatorPage.vue";
import LinkPage from "@/views/link/LinkPage.vue";
import InputLinkPage from "@/views/link/InputLinkPage.vue";

const routes: Array<RouteRecordRaw> = [
    {
        path: "/login",
        name: "Login",
        component: Login,
    },
    {
        path: "/",
        name: "Dashboard",
        component: Dashboard,
        children: [
            {
                path: "/",
                name: "Home",
                component: Home,
            },
            {
                path: "/client",
                name: "Client",
                component: ClientPage,
            },
            {
                path: "/contact-set",
                name: "Contact Set",
                component: ContactSetPage,
            },
            {
                path: "/contact/:id",
                name: "Contact",
                component: ContactPage,
            },
            {
                path: "/broadcast",
                name: "Broadcast",
                component: BroadcastPage,
            },
            {
                path: "/broadcast/add",
                name: "Add Broadcast",
                component: InputBroadcastPage,
            },
            {
                path: "/broadcast/edit/:id",
                name: "Edit Broadcast",
                component: InputBroadcastPage,
            },
            {
                path: "/broadcast/detail/:id",
                name: "Broadcast Detail",
                component: DetailBroadcastPage,
            },
            {
                path: "/operator",
                name: "Operator",
                component: OperatorPage,
            },
            {
                path: "/link",
                name: "Link",
                component: LinkPage,
            },
            {
                path: "/link/add",
                name: "Add Link",
                component: InputLinkPage,
            },
            {
                path: "/link/edit/:id",
                name: "Edit Link",
                component: InputLinkPage,
            },
        ]
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

export default router
