import Mapper from "./Mapper";

export default interface ContactSetModel {
    id: string,
    userId: string,
    name: string,
    contactCount: number,
    createdAt: string,
    updatedAt: string,
}

export const ContactSetModelMapper: Mapper<ContactSetModel> = new class extends Mapper<ContactSetModel> {
    map = {
        "id" : "id",
        "user_id" : "userId",
        "name" : "name",
        "contact_count" : "contactCount",
        "created_at" : "createdAt",
        "updated_at" : "updatedAt",
    }
}()