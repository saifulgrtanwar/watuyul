import Mapper from "./Mapper";

export default interface BroadcastContentModel {
    text: string,
    image?: string,
    video?: string,
}

export const BroadcastContentModelMapper: Mapper<BroadcastContentModel> = new class extends Mapper<BroadcastContentModel> {
    map = {
        "text" : "text",
        "image" : "image",
        "video" : "video",
    }
}()