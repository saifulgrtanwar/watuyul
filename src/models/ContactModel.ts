import Mapper from "./Mapper";

export default interface ContactModel {
    id: string,
    contactSetId: string,
    userId: string,
    name: string,
    number: string,
    createdAt: string,
    updatedAt: string,
}

export const ContactModelMapper: Mapper<ContactModel> = new class extends Mapper<ContactModel> {
    map = {
        "id" : "id",
        "contact_set_id" : "contactSetId",
        "user_id" : "userId",
        "name" : "name",
        "number" : "number",
        "created_at" : "createdAt",
        "updated_at" : "updatedAt",
    }
}()