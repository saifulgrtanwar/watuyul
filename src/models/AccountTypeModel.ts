import Mapper from "./Mapper";

export default interface AccountTypeModel {
    id: string,
    type: string,
    maxClient: number,
    maxContactSet: number,
    maxContact: number,
    maxBroadcast: number,
    maxMessage: number,
    maxOperator: number,
    maxLink: number
}

export const AccountTypeModelMapper: Mapper<AccountTypeModel> = new class extends Mapper<AccountTypeModel> {
    map = {
        "id": "id",
        "type": "name",
        "max_client": "maxClient",
        "max_contact_set": "maxContactSet",
        "max_contact": "maxContact",
        "max_broadcast": "maxBroadcast",
        "max_message": "maxMessage",
        "max_operator": "maxOperator",
        "max_link": "maxLink",
    }
}()