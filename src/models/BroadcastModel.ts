import Mapper from "./Mapper";
import ClientModel, {ClientModelMapper} from "@/models/ClientModel";
import ContactSetModel, {ContactSetModelMapper} from "@/models/ContactSetModel";
import BroadcastContentModel, {BroadcastContentModelMapper} from "@/models/BroadcastContentModel";

export default interface BroadcastModel {
    id: string,
    name: string,
    userId: string,
    clientId: string,
    client: ClientModel,
    contactSet: Array<ContactSetModel>,
    contents: Array<BroadcastContentModel>,
    sendOption: string,
    sendTime: string | null,
    status: string,
    maxMessagePerRequest: number,
    maxMessagePerMinute: number,
    totalContacts: number,
    totalMessages: number,
    pendingMessages: number,
    sentMessages: number,
    notSentMessages: number,
    createdAt: string,
    updatedAt: string,
}

export const BroadcastModelMapper: Mapper<BroadcastModel> = new class extends Mapper<BroadcastModel> {
    map = {
        "id" : "id",
        "name" : "name",
        "user_id" : "userId",
        "client_id" : "clientId",
        "client" : ["client", (data: any) => ClientModelMapper.mapObject(data)],
        "contact_set" : ["contactSet", (data: any) => ContactSetModelMapper.mapArray(data)],
        "contents" : ["contents", (data: any) => BroadcastContentModelMapper.mapArray(data)],
        "send_option" : "sendOption",
        "send_time" : "sendTime",
        "status" : "status",
        "max_message_per_request" : "maxMessagePerRequest",
        "max_message_per_minute" : "maxMessagePerMinute",
        "total_contacts" : "totalContacts",
        "total_messages" : "totalMessages",
        "pending_messages" : "pendingMessages",
        "sent_messages" : "sentMessages",
        "not_sent_messages" : "notSentMessages",
        "created_at" : "createdAt",
        "updated_at" : "updatedAt",
    }
}()