import Mapper from "./Mapper";

export default interface ClientModel {
    id: string,
    name: string,
    status: string,
}

export const ClientModelMapper: Mapper<ClientModel> = new class extends Mapper<ClientModel> {
    map = {
        "id" : "id",
        "name" : "name",
        "status" : "status",
    }
}()