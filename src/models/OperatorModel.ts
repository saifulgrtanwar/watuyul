import Mapper from "./Mapper";

export default interface OperatorModel {
    id: string,
    name: string,
    number: string,
    clicks: number,
    links: number,
    createdAt: string,
    updatedAt: string,
}

export const OperatorModelMapper: Mapper<OperatorModel> = new class extends Mapper<OperatorModel> {
    map = {
        "id" : "id",
        "name" : "name",
        "number" : "number",
        "clicks" : "clicks",
        "links" : "links",
        "created_at" : "createdAt",
        "updated_at" : "updatedAt",
    }
}()