import Mapper from "./Mapper";

interface UserModel {
    id: string,
    name: string,
    email: string,
}

export default UserModel

export const UserModelMapper : Mapper<UserModel> = new class extends Mapper<UserModel>{
    map = {
        "id" : "id",
        "name" : "name",
        "email" : "email",
    }
}()