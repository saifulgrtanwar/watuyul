import Logger from "../api/Logger";

/*
Base class untuk mapping dari respon API (JSON) menjadi model class T
 */
export default abstract class Mapper<T> {
    /*
    Object yg digunakan untuk mapping.
    Wajib dioverride oleh child class

    Contoh format mapnya:
    {
        "field_dari_api": "fieldDiObjectnya",
        "field_lagi": (data: any) => ["fieldDiObjectnyaJuga", hitungKomputasiAtauMapKeObjekLain()]
    }
     */
    abstract map: any

    /*
    Fungsi kosong, dapat di override oleh child class.
    Untuk inisialisasi data.
     */
    init(data: T) {}

    /*
    Fungsi kosong, dapat di override oleh child class.
    Untuk custom JSON.
     */
    jsonizeInit(data: T, obj: any) {}

    /*
    Mapping dari JSON menjadi object T
     */
    mapObject(data: any): T {
        // Bikin objek kosongan dulu
        const obj = {} as any

        // Loop through map yg dioverride child class
        for (const key in this.map) {
            try {
                // Data input
                const input = data[key]
                // Data output
                const output = this.map[key]

                // Kalau outputnya string langsung masukin aja data inputnya ke obj
                if (typeof output === "string") {
                    obj[output] = input
                }
                // Kalau outputnya function, punggil functionnya baru masukin hasilnya ke obj
                else if (typeof output === "object") {
                    const [k, v] = output
                    obj[k] = v(input)
                }
                else {
                    Logger.i("type " + typeof output)
                }
            }
            catch (e) {
                Logger.e("Gak bisa parsing key : " + key)
            }
        }

        // Panggil fungsi init, kali aja child class nya pengen inisialisasi data
        this.init(obj as T)

        return obj as T
    }

    /*
    Mapping dari JSON menjadi array T
     */
    mapArray(data: any): Array<T> {
        const arr = [] as any

        if (data) {
            data.forEach((x: any) => {
                arr.push(this.mapObject(x))
            })
        }

        return arr as Array<T>
    }

    /*
    Mapping dari object T ke JSON Object
     */
    toJsonObject(t: T) {
        const data = t as any
        const obj = {} as any

        // Loop through map yg dioverride child class
        for (const key in this.map) {
            try {
                const objKey = this.map[key]

                if (typeof objKey === "string") {
                    obj[key] = data[objKey]
                }
                else if (typeof objKey === "object") {
                    obj[key] = data[objKey[0]]
                }
            }
            catch (e) {
                Logger.e("Gak bisa stringify key : " + key)
            }
        }

        this.jsonizeInit(t, obj)

        return obj
    }

    /*
    Mapping dari array T ke JSON Array
     */
    toJsonArray(tarr: Array<T>) {
        const arr = [] as any

        if (tarr) {
            tarr.forEach((x: any) => {
                arr.push(this.toJsonObject(x))
            })
        }

        return arr as Array<any>
    }
}