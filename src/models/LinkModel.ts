import Mapper from "./Mapper";

export interface OperatorPriority {
    id: string,
    name: string,
    priority: number
}

export default interface LinkModel {
    id: string,
    title: string,
    slug: string,
    operators: Array<OperatorPriority>,
    chatContent: string,
    status: string,
    pixelId: string,
    pixelEvent: string,
    pixelEventData: string,
    gtmId: string,
    loading: number,
    clicks: number,
    createdAt: string,
    updatedAt: string,
}

export const LinkModelMapper: Mapper<LinkModel> = new class extends Mapper<LinkModel> {
    map = {
        "id" : "id",
        "title" : "title",
        "slug" : "slug",
        "operators" : "operators",
        "chat_content" : "chatContent",
        "status" : "status",
        "pixel_id" : "pixelId",
        "pixel_event" : "pixelEvent",
        "pixel_event_data" : "pixelEventData",
        "gtm_id" : "gtmId",
        "loading" : "loading",
        "clicks" : "clicks",
        "created_at" : "createdAt",
        "updated_at" : "updatedAt",
    }
}()