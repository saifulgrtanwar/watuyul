import Mapper from "./Mapper";
import ClientModel, {ClientModelMapper} from "@/models/ClientModel";
import ContactSetModel, {ContactSetModelMapper} from "@/models/ContactSetModel";
import BroadcastContentModel, {BroadcastContentModelMapper} from "@/models/BroadcastContentModel";

export default interface MessageModel {
    id: string,
    userId: string,
    clientId: string,
    client: ClientModel,
    name: string,
    number: string,
    message: string,
    sendTime: string,
    status: string,
    createdAt: string,
    updatedAt: string,
}

export const MessageModelMapper: Mapper<MessageModel> = new class extends Mapper<MessageModel> {
    map = {
        "id" : "id",
        "user_id" : "userId",
        "client_id" : "clientId",
        "client" : ["client", (data: any) => ClientModelMapper.mapObject(data)],
        "name" : "name",
        "number" : "number",
        "message" : "message",
        "send_time" : "sendTime",
        "status" : "status",
        "created_at" : "createdAt",
        "updated_at" : "updatedAt",
    }
}()