import {_delete, get, post, put} from "@/api/Axios";
import {LinkModelMapper, OperatorPriority} from "@/models/LinkModel";

export default class LinkApi {
    static getAll() {
        return get('link')
            .then(r => LinkModelMapper.mapArray(r))
    }

    static getById(id: string) {
        return get('link/' + id)
            .then(r => LinkModelMapper.mapObject(r))
    }

    static create(
        title: string, slug: string,
        operators: Array<OperatorPriority>, chatContent: string,
        status: string,
        pixelId: string, pixelEvent: string, pixelEventData: string, gtmId: string, loading: number,
    ) {
        return post('link/', {
            title, slug, operators, chat_content: chatContent, status,
            pixel_id: pixelId, pixel_event: pixelEvent, pixel_event_data: pixelEventData, gtm_id: gtmId, loading,
        })
            .then(r => LinkModelMapper.mapObject(r))
    }

    static update(
        id: string, title: string, slug: string,
        operators: Array<OperatorPriority>, chatContent: string,
        status: string,
        pixelId: string, pixelEvent: string, pixelEventData: string, gtmId: string, loading: number,
    ) {
        return put('link/' + id, {
            title, slug, operators, chat_content: chatContent, status,
            pixel_id: pixelId, pixel_event: pixelEvent, pixel_event_data: pixelEventData, gtm_id: gtmId, loading,
        })
            .then(r => LinkModelMapper.mapObject(r))
    }

    static delete(id: string) {
        return _delete('link/' + id)
            .then(r => r)
    }

    static deleteMany(ids: Array<string>) {
        return _delete('link/many', {
            ids
        })
            .then(r => r)
    }
}