import {_delete, get, post, put} from "@/api/Axios";
import {ClientModelMapper} from "@/models/ClientModel";

export default class ClientApi {
    static getAll() {
        return get('client')
            .then(r => ClientModelMapper.mapArray(r))
    }

    static getById(id: string) {
        return get('client/' + id)
            .then(r => ClientModelMapper.mapObject(r))
    }

    static create(name: string) {
        return post('client/', {
            name
        })
            .then(r => ClientModelMapper.mapObject(r))
    }

    static update(id: string, name: string) {
        return put('client/' + id, {
            name
        })
            .then(r => ClientModelMapper.mapObject(r))
    }

    static use(id: string) {
        return post('client/use/' + id, {})
            .then(r => ClientModelMapper.mapObject(r))
    }

    static release(id: string) {
        return post('client/release/' + id, {})
            .then(r => ClientModelMapper.mapObject(r))
    }

    static delete(id: string) {
        return _delete('client/' + id)
            .then(r => r)
    }

    static deleteMany(ids: Array<string>) {
        return _delete('client/many', {
            ids
        })
            .then(r => r)
    }
}