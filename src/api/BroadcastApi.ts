import {_delete, get, post, put} from "@/api/Axios";
import {BroadcastModelMapper} from "@/models/BroadcastModel";
import BroadcastContentModel from "@/models/BroadcastContentModel";
import {MessageModelMapper} from "@/models/MessageModel";

export default class BroadcastApi {
    static getAll() {
        return get('broadcast')
            .then(r => BroadcastModelMapper.mapArray(r))
    }

    static getMessages(id: string) {
        return get('message/by_broadcast/' + id)
            .then(r => MessageModelMapper.mapArray(r))
    }

    static getById(id: string) {
        return get('broadcast/' + id)
            .then(r => BroadcastModelMapper.mapObject(r))
    }

    static create(
        name: string, client_id: string,
        contact_set_ids: Array<string>, contents: Array<BroadcastContentModel>,
        send_option: string, send_time: string | null, status: string,
        max_message_per_request: number, max_message_per_minute: number
    ) {
        return post('broadcast/', {
            name, client_id, contact_set_ids, contents, send_option, send_time, status,
            max_message_per_request, max_message_per_minute
        })
            // .then(r => BroadcastModelMapper.mapObject(r))
    }

    static update(
        id: string,
        name: string, contents: Array<BroadcastContentModel>,
        send_option: string, send_time: string | null, status: string,
        max_message_per_request: number, max_message_per_minute: number
    ) {
        return put('broadcast/' + id, {
            name, contents, send_option, send_time, status,
            max_message_per_request, max_message_per_minute
        })
            .then(r => BroadcastModelMapper.mapObject(r))
    }

    static delete(id: string) {
        return _delete('broadcast/' + id)
            .then(r => r)
    }

    static deleteMany(ids: Array<string>) {
        return _delete('broadcast/many', {
            ids
        })
            .then(r => r)
    }
}