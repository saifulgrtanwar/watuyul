import {_delete, get, post, put} from "@/api/Axios";
import {ContactModelMapper} from "@/models/ContactModel";

export default class ContactApi {
    static getAll() {
        return get('contact')
            .then(r => ContactModelMapper.mapArray(r))
    }

    static getBySet(id: string) {
        return get('contact/by_set/' + id)
            .then(r => ({
                totalContact: r.total_contact,
                contact: ContactModelMapper.mapArray(r.contact)
            }))
    }

    static getById(id: string) {
        return get('contact/' + id)
            .then(r => ContactModelMapper.mapObject(r))
    }

    static create(contactSetId: string, name: string, number: string) {
        return post('contact/', {
            contact_set_id: contactSetId, name, number
        })
            .then(r => ContactModelMapper.mapObject(r))
    }

    static createMany(contactSetId: string, items: Array<{name: string, number: string}>) {
        return post('contact/many', {
            contact_set_id: contactSetId, items
        })
            .then(r => ContactModelMapper.mapObject(r))
    }

    static update(id: string, name: string, number: string) {
        return put('contact/' + id, {
            name, number
        })
            .then(r => ContactModelMapper.mapObject(r))
    }

    static delete(id: string) {
        return _delete('contact/' + id)
            .then(r => r)
    }

    static deleteMany(ids: Array<string>) {
        return _delete('contact/many', {
            ids
        })
            .then(r => r)
    }
}