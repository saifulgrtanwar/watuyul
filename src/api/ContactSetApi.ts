import {_delete, get, post, put} from "@/api/Axios";
import {ContactSetModelMapper} from "@/models/ContactSetModel";

export default class ContactSetApi {
    static getAll() {
        return get('contact_set')
            .then(r => ContactSetModelMapper.mapArray(r))
    }

    static getById(id: string) {
        return get('contact_set/' + id)
            .then(r => ContactSetModelMapper.mapObject(r))
    }

    static create(name: string) {
        return post('contact_set/', {
            name
        })
            .then(r => ContactSetModelMapper.mapObject(r))
    }

    static update(id: string, name: string) {
        return put('contact_set/' + id, {
            name
        })
            .then(r => ContactSetModelMapper.mapObject(r))
    }

    static delete(id: string) {
        return _delete('contact_set/' + id)
            .then(r => r)
    }

    static deleteMany(ids: Array<string>) {
        return _delete('contact_set/many', {
            ids
        })
            .then(r => r)
    }
}