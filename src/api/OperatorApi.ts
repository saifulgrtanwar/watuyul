import {_delete, get, post, put} from "@/api/Axios";
import {OperatorModelMapper} from "@/models/OperatorModel";

export default class OperatorApi {
    static getAll() {
        return get('operator')
            .then(r => OperatorModelMapper.mapArray(r))
    }

    static getById(id: string) {
        return get('operator/' + id)
            .then(r => OperatorModelMapper.mapObject(r))
    }

    static create(name: string, number: string) {
        return post('operator/', {
            name, number
        })
            .then(r => OperatorModelMapper.mapObject(r))
    }

    static update(id: string, name: string, number: string) {
        return put('operator/' + id, {
            name, number
        })
            .then(r => OperatorModelMapper.mapObject(r))
    }

    static delete(id: string) {
        return _delete('operator/' + id)
            .then(r => r)
    }

    static deleteMany(ids: Array<string>) {
        return _delete('operator/many', {
            ids
        })
            .then(r => r)
    }
}