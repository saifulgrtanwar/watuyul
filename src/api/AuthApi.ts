import {_delete, get, post, put} from "@/api/Axios";
import {ClientModelMapper} from "@/models/ClientModel";
import {AccountTypeModelMapper} from "@/models/AccountTypeModel";

export default class AuthApi {
    static login(email: string, password: string) {
        return post('login', {email, password})
    }

    static getAll() {
        return get('client')
            .then(r => ClientModelMapper.mapArray(r))
    }

    static getById(id: string) {
        return get('client/' + id)
            .then(r => ClientModelMapper.mapObject(r))
    }

    static getAccountType() {
        return get('user/account_type')
            .then(r => AccountTypeModelMapper.mapObject(r))
    }

    static create(name: string) {
        return post('client/', {
            name
        })
            .then(r => ClientModelMapper.mapObject(r))
    }

    static update(name: string) {
        return put('client/', {
            name
        })
            .then(r => ClientModelMapper.mapObject(r))
    }

    static delete(id: string) {
        return _delete('client/' + id)
            .then(r => r)
    }

    static deleteMany(ids: Array<string>) {
        return _delete('client/many', {
            ids
        })
            .then(r => r)
    }
}