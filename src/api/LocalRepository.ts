import UserModel from "@/models/UserModel";

export default class LocalRepository {
    static isLogin() {
        return this.getUser() != null
    }

    static setUser(user: UserModel) {
        localStorage.setItem("user", JSON.stringify(user))
    }

    static setToken(token: string) {
        localStorage.setItem("token", token)
    }

    static getUser(): UserModel | null {
        const u = localStorage.getItem("user")

        if (!u)
            return null
        else
            return JSON.parse(u)
    }

    static getToken(): string | null {
        return  localStorage.getItem("token")
    }

    static logout() {
        localStorage.removeItem('token')
        localStorage.removeItem('user')
    }
}