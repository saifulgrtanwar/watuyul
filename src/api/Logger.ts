/*
Nanti biar loggingnya gak aktif tinggal diset jadi false aja
 */
const LOG_ENABLED = true
const COMPUTE = true

/*
Class yg membungkus fungsi console.log()
 */
export default class Logger {
    private static compute(d: any) {
        if (typeof d == "object" && COMPUTE)
            return JSON.parse(JSON.stringify(d))
        else
            return d
    }

    static d(message: any) {
        if (LOG_ENABLED)
            console.log(message)
    }

    static i(message: any) {
        if (LOG_ENABLED)
            console.info(message)
    }

    static w(message: any) {
        if (LOG_ENABLED)
            console.warn(message)
    }

    static e(message: any) {
        if (LOG_ENABLED)
            console.error(message)
    }
}