import axios, {AxiosInstance, AxiosRequestConfig} from "axios";
import Logger from "@/api/Logger";
import LocalRepository from "@/api/LocalRepository";
import store from '@/store'

// export const BASE_URL = "http://192.168.43.183:8000/"
// export const BASE_URL = "http://localhost:8000/"
export const BASE_URL = "https://api.watuyul.com/"

/*
Instance axios yg buat GET, POST, PUT, DELETE
 */
let Axios: AxiosInstance | null = null

function getAxios() {
    if (Axios == null) {
        Axios = axios.create({
            baseURL: BASE_URL,
            timeout: 30000,
            headers: {
                'Content-Type': 'application/json'
            }
        })

        Axios.interceptors.request.use(config => {
            if (LocalRepository.getToken())
                config.headers.Authorization = "Bearer " + LocalRepository.getToken()

            return config
        })
    }

    return Axios
}

/*
Membungkus fungsi post dari axios.
 */
export function post(url: string, data: any) {
    Logger.d("API POST " + url + " with  data : ")
    Logger.d(data)

    store.dispatch('incrementApiCall')

    return getAxios().post(url, data)
        .then(r => {
            return parse(r, "POST" + url)
        })
        .catch(e => {
            if (e.response && e.response.data && e.response.data.message) {
                throw new Error(e.response.data.message)
            }
            else {
                throw new Error(e.message)
            }
        })
        .finally(() => {
            store.dispatch('decrementApiCall')
        })
}

/*
Membungkus fungsi put dari axios.
 */
export function put(url: string, data: any) {
    Logger.d("API PUT " + url + " with data : ")
    Logger.d(data)

    store.dispatch('incrementApiCall')

    return getAxios().put(url, data)
        .then(r => {
            return parse(r, "PUT " + url)
        })
        .catch(e => {
            if (e.response && e.response.data && e.response.data.message) {
                throw new Error(e.response.data.message)
            }
            else {
                throw new Error(e.message)
            }
        })
        .finally(() => {
            store.dispatch('decrementApiCall')
        })
}

export function _delete(url: string, data?: any) {
    Logger.d("API DELETE " + url + " with data : ")
    Logger.d(data)

    store.dispatch('incrementApiCall')

    return getAxios().delete(url, {
        data
    })
        .then(r => {
            return parse(r, "DELETE " + url)
        })
        .catch(e => {
            if (e.response && e.response.data && e.response.data.message) {
                throw new Error(e.response.data.message)
            }
            else {
                throw new Error(e.message)
            }
        })
        .finally(() => {
            store.dispatch('decrementApiCall')
        })
}

/*
Membungkus fungsi get dari axios.
 */
export function get(url: string) {
    Logger.d("API GET " + url)

    store.dispatch('incrementApiCall')

    return getAxios().get(url)
        .then(r => {
            return parse(r, "GET " + url)
        })
        .catch(e => {
            if (e.response && e.response.data && e.response.data.message) {
                throw new Error(e.response.data.message)
            }
            else {
                throw new Error(e.message)
            }
        })
        .finally(() => {
            store.dispatch('decrementApiCall')
        })
}

/*
Respon dari API dicek, kalau dari API merespon dg kode error, maka akan nge throw error.
Sekalian nge logging hasilnya.
Kalau sukses, baik dari axios maupun dari API nya, akan diambil yg bagian "data" nya aja.

Contoh Respon dari API :

{
    "success": "true"
    "data": {
        "anu": 12,
        "itu": "semaumu"
    }
}

Maka hasilnya kan jadi :

{
    "anu": 12,
    "itu": "semaumu"
}

 */
function parse(r: any, url: string): any {
    if (!r.data.success) {
        Logger.e("Error dari API " + url)
        Logger.e(r.data)
        throw new Error(r.data.message)
    }
    else {
        Logger.i("Success dari API " + url)
        Logger.i(r.data)
    }

    return r.data.data
}

export default Axios