import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import './utils/Extensions'

createApp(App)
    .use(store)
    .use(router)
    .use(ElementPlus)
    .use(VueSweetalert2)
    .mount("#app");
